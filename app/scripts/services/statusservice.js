'use strict';

/**
 * @ngdoc service
 * @name micoApp.StatusService
 * @description
 * # StatusService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('StatusService', function (ENV,$http,$q,MessageService) {
    //TODO implement
    this.getStatus = function(uri) {
      var deferred = $q.defer();

      $http.get(ENV.statusServicePath,{params:{parts:false,uri:uri}}).then(function(data){
        deferred.resolve(data.data[0])
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    }
  });
