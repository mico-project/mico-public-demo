'use strict';

/**
 * @ngdoc service
 * @name micoApp.sampleService
 * @description
 * # sampleService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('SampleService', function (Sample) {

    function shuffleArray(array) {
      for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
      }
      return array;
    }

    var sampleVideos = [
      new Sample('http://demo3.mico-project.eu:8080/marmotta/cea653dc-2002-47bf-bf76-a44e9fb32695','video','http://demo3.mico-project.eu/rest/download?partUri=http%3A%2F%2Fdemo3.mico-project.eu%3A8080%2Fmarmotta%2F32f61cb6-c5ed-48c4-a767-b5d91609e3bf&itemUri=http%3A%2F%2Fdemo3.mico-project.eu%3A8080%2Fmarmotta%2Fcea653dc-2002-47bf-bf76-a44e9fb32695'),
      new Sample('http://demo3.mico-project.eu:8080/marmotta/45ad9afa-2eb4-4818-a190-ad7b925fe878','video','http://demo3.mico-project.eu/rest/download?partUri=http%3A%2F%2Fdemo3.mico-project.eu%3A8080%2Fmarmotta%2Fcc6450fb-58bf-4887-9c91-58a7cba2442a&itemUri=http%3A%2F%2Fdemo3.mico-project.eu%3A8080%2Fmarmotta%2F45ad9afa-2eb4-4818-a190-ad7b925fe878')
    ];

    var sampleImages = [
      new Sample('http://demo3.mico-project.eu:8080/marmotta/7d125348-3fe1-4669-9668-9b936efe34a3','image','http://demo3.mico-project.eu/rest/download?itemUri=http://demo3.mico-project.eu:8080/marmotta/7d125348-3fe1-4669-9668-9b936efe34a3'),
      new Sample('http://demo3.mico-project.eu:8080/marmotta/a6c569a4-be07-4257-b086-d1face1ff00a','image','http://demo3.mico-project.eu/rest/download?itemUri=http://demo3.mico-project.eu:8080/marmotta/a6c569a4-be07-4257-b086-d1face1ff00a')
    ];

    var sampleTexts = [
      new Sample('http://demo3.mico-project.eu:8080/marmotta/fb3f12a6-9b6b-440f-8be9-f4eb88eb083a','text','images/fake/text-mico.png'),
      new Sample('http://demo3.mico-project.eu:8080/marmotta/edf39858-70bd-4e9e-8f29-4e8cdb8fabf5','text','images/fake/text-welt.png')
    ];

    var samples = shuffleArray(sampleVideos.concat(sampleImages,sampleTexts));

    this.listSamples = function() {
      return samples;
    };

    this.getRandomSample = function() {
      return samples[Math.floor(Math.random() * samples.size)];
    }

  });
