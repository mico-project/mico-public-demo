'use strict';

/**
 * @ngdoc service
 * @name micoApp.ImageAnalysisService
 * @description
 * # ImageAnalysisService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('ImageAnalysisService', function ($q, SparqlService, $http, ENV) {

    var animalDetectionQuery = "PREFIX mico: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n"+
      "PREFIX mmterms: <http://www.mico-project.eu/ns/mmmterms/2.0/schema#>\n"+
      "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"+
      "SELECT ?label ?region ?confidence WHERE {\n"+
      "<{{uri}}> mico:hasPart ?annot.\n"+
      "?annot mico:hasBody ?body.\n"+
      "?body rdf:type mmterms:AnimalDetectionBody;\n"+
      " rdf:value ?label;\n"+
      " mico:hasConfidence ?confidence.\n"+
      "?annot mico:hasTarget/mico:hasSelector/rdf:value ?region." +
      "}\n";

    var facesQuery = "PREFIX dct: <http://purl.org/dc/terms/>\n" +
      "PREFIX mico: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
      "SELECT DISTINCT ?region WHERE {\n" +
      "   <{{uri}}> mico:hasPart ?c.\n" +
      "   ?c mico:hasTarget/oa:hasSelector/rdf:value ?region.\n" +
      "   ?c mico:hasBody/rdf:type <http://www.mico-project.eu/ns/mmmterms/2.0/schema#FaceDetectionBody>\n" +
      "}";

    function getSpatialFragment(uri) {
      var match = /(#|\?|\&)xywh=(-?\d+(.\d+)?),(-?\d+(.\d+)?),(-?\d+(.\d+)?),(-?\d+(.\d+)?)/.exec(uri);
      if(match) {
        return {
          "x":match[2],
          "y":match[4],
          "w":match[6],
          "h":match[8]
        }
      } else return uri;
    }

    this.getAnalysisResult = function(uri) {
      var deferred = $q.defer();

      var getAnimals = SparqlService.query('List Animals',animalDetectionQuery.replace(/\{\{uri\}\}/,uri), ['label','region','confidence'],{region:getSpatialFragment},true);
      var getFaces = SparqlService.query('List Faces',facesQuery.replace(/\{\{uri\}\}/,uri), ['region'],{region:getSpatialFragment},true);

      $q.all([
        getAnimals,
        getFaces
      ]).then(function(results) {
        var result = {
          animals: results[0],
          faces: results[1]
        };
        deferred.resolve(result);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };

  });
