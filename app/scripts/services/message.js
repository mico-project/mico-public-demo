'use strict';

/**
 * @ngdoc service
 * @name micoApp.message
 * @description
 * # message
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('MessageService', function ($rootScope, $timeout) {

    $rootScope.alerts = [];

    var self = this;

    /**
     *
     * @param text
     * @param type success, info, warning, danger (default:info)
     * @param timeout (default:undefined)
     * @param id (optional) to prevent having the same message twice
     */
    this.addMessage = function(text,type,timeout,id) {
      var message = {msg:text};

      if(id) {
        removeMessage(id);
        message.id=id;
      } else {
        message.id=Math.random().toString(36).substring(7);
      }

      message.type = type ?  type : 'info';

      $rootScope.alerts.push(message);

      if(timeout) {
        $timeout(function(){
          self.removeMessage(message);
        }, timeout)
      }

    };

    function removeMessage(id) {
      var _index = -1;
      angular.forEach($rootScope.alerts, function(value,index){
        if(value.id == id) _index = index;
      });
      $rootScope.alerts.splice(_index, 1);
    }

    this.removeMessage = function(message) {
      removeMessage(message.id);
    }

  });
