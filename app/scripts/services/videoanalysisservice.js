'use strict';

/**
 * @ngdoc service
 * @name micoApp.VideoAnalysisService
 * @description
 * # VideoAnalysisService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('VideoAnalysisService', function (SparqlService, $q) {
    var videoSegments = "PREFIX mm: <http://linkedmultimedia.org/sparql-mm/ns/2.0.0/function#>\n" +
      "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
      "PREFIX mico: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "PREFIX mmterms: <http://www.mico-project.eu/ns/mmmterms/2.0/schema#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "SELECT DISTINCT ?image ?range ?duration WHERE {\n" +
      " <{{uri}}> mico:hasPart ?part1." +
      " ?part1 mico:hasBody/rdf:type mmterms:TVSShotBody.?part1 mico:hasTarget/oa:hasSelector/rdf:value  ?range.\n" +
      " <{{uri}}> mico:hasPart ?image.\n" +
      " ?image mico:hasBody/rdf:type mmterms:TVSShotBoundaryFrameBody.\n" +
      " ?image mico:hasTarget/oa:hasSelector/rdf:value ?range2;\n" +
      "   mico:hasSyntacticalType \"mico:Image\"\n" +
      " FILTER (mm:start(?range) = mm:start(?range2))\n" +
      " BIND (mm:duration(?range) AS ?duration)\n" +
      "} ORDER BY mm:start(?range)";

    function getNPTFragment(uri) {
      var match = /#t=npt:(\d+.\d+),(\d+.\d+)/.exec(uri);
      if(match) {
        return {
          "start":match[1],
          "end":match[2]
        }
      } else return uri;
    }

    this.getAnalysisResult = function(uri) {
      var deferred = $q.defer();

      var getTVS = SparqlService.query('Get Segments',videoSegments.replace(/\{\{uri\}\}/g,uri), ['image','range','duration'],{range:getNPTFragment},true);

      $q.all([
        getTVS
      ]).then(function(results) {
        var result = {
          segments: results[0]
        };
        deferred.resolve(result);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };
  });
