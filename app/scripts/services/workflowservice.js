'use strict';

/**
 * @ngdoc service
 * @name micoApp.WorkflowService
 * @description
 * # WorkflowService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('WorkflowService', function ($http, $q, ENV) {

    var self = this;

    /*var fixedWorkflows = [
      {id:'1',workflowName:'Serengeti-RedlinkNER (text)'},
      {id:'2',workflowName:'Kaldi Speech2Text (video,NER)'},
      {id:'3',workflowName:'Face detection (image)'},
      {id:'4',workflowName:'Face detection (video-keyframes)'},
      {id:'5',workflowName:'Face detection (video-steps)'},
      {id:'6',workflowName:'IO-demo showcase all (video)'},
      {id:'7',workflowName:'Animal detection (image)'}
    ];*/

    this.listWorkflows = function() {

      var deferred_outer = $q.defer();

      $http.get(ENV.listWorkflowsPath,{params:{"user":"0"}})
        .then(
        function (response) {

          var deferred = [];

          angular.forEach(response.data, function(id){
              deferred.push(self.getWorkflow(id));
          });

          $q.all(deferred).then(function(data){
            //data = data.concat(fixedWorkflows);
            deferred_outer.resolve(data);
          });
        });

      return deferred_outer.promise;
    };

    this.getWorkflow = function(id) {
      var deferred = $q.defer();

      //hack
      /*if(id > 0 && id < 8) {
        deferred.resolve(fixedWorkflows[id-1]);
      }*/


      $http.get(ENV.statusWorkflowPath + '/' + id)
        .then(
        function (response) {
          var status = response.data;
          $http.get(ENV.getWorkflowPath + '/' + id).then( function(rsp){
            rsp.data.id = id;
            rsp.data.status = status;
            deferred.resolve(rsp.data);
          },function(error) {
            deferred.reject(error.data);
          });
        },function(error) {
          deferred.reject(error.data);
        });

      return deferred.promise;
    }

  });
