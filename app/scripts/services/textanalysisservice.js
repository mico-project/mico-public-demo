'use strict';

/**
 * @ngdoc service
 * @name micoApp.TextService
 * @description
 * # TextService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('TextAnalysisService', function ($q, SparqlService, $http, ENV, $rootScope) {

    var languageQuery = "PREFIX fam: <http://vocab.fusepool.info/fam#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "PREFIX mmm: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "PREFIX dct: <http://purl.org/dc/terms/>\n" +
      "SELECT ?language WHERE {\n" +
      "  <{{uri}}>\n" +
      "  mmm:hasPart/oa:hasBody ?body.\n" +
      "  ?body a fam:LanguageAnnotation; dct:language ?language.\n" +
      "}";

    var sentimentQuery = "PREFIX fam: <http://vocab.fusepool.info/fam#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "PREFIX mmm: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "SELECT ?sentiment WHERE {\n" +
      "  <{{uri}}>\n" +
      "  mmm:hasPart/oa:hasBody ?body.\n" +
      "  ?body a fam:SentimentAnnotation; fam:sentiment ?sentiment.\n" +
      "}";

    var topicsQuery = "PREFIX fam: <http://vocab.fusepool.info/fam#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "PREFIX mmm: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "SELECT DISTINCT ?uri ?label ?confidence WHERE {\n" +
      "  <{{uri}}>\n" +
      "  mmm:hasPart/oa:hasBody ?body.\n" +
      "  ?body a fam:TopicAnnotation; fam:topic-label ?label; fam:topic-reference ?uri; fam:confidence ?confidence.\n" +
      "} ORDER BY DESC(?confidence)";

    var entitiesQuery = "PREFIX fam: <http://vocab.fusepool.info/fam#>\n" +
      "PREFIX oa: <http://www.w3.org/ns/oa#>\n" +
      "PREFIX mmm: <http://www.mico-project.eu/ns/mmm/2.0/schema#>\n" +
      "SELECT DISTINCT ?uri ?label ?confidence ?start ?end ?type WHERE {\n" +
      "  <{{uri}}>\n" +
      "  mmm:hasPart/oa:hasBody ?body.\n" +
      "  ?body a fam:LinkedEntity; fam:entity-label ?label; fam:entity-reference ?uri; fam:confidence ?confidence; fam:selector ?selector. ?selector oa:start ?start; oa:end ?end.\n" +
      "  OPTIONAL {?body fam:entity-type ?type}} ORDER BY DESC(?confidence)";

    this.getAnalysisResult = function(uri) {
      var deferred = $q.defer();

      var getSentiment = SparqlService.query('Get Sentiment',sentimentQuery.replace(/\{\{uri\}\}/,uri), ['sentiment'],{sentiment:parseFloat});
      var getTopics = SparqlService.query('List Topics',topicsQuery.replace(/\{\{uri\}\}/,uri), ['uri','label','confidence'],{confidence:parseFloat},true);
      var getEntities = SparqlService.query('List Entities',entitiesQuery.replace(/\{\{uri\}\}/,uri), ['uri','label','type','confidence','start','end'],{confidence:parseFloat,start:parseInt,end:parseInt},true);
      var getLanguage = SparqlService.query('Get Language',languageQuery.replace(/\{\{uri\}\}/,uri), ['language']);

      $q.all([
        getSentiment,
        getTopics,
        getEntities,
        getLanguage
      ]).then(function(results) {
        var result = {
          sentiment: results[0],
          topics: results[1],
          entities: results[2],
          language: results[3]
        };
        deferred.resolve(result);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };

    this.getText = function(uri) {
      var deferred = $q.defer();

      $rootScope.$broadcast('request',{type:'Content',label:'Get Text',query:ENV.itemDownloadPath + '?itemUri='+encodeURIComponent(uri)});

      $http({
        method: 'GET',
        url: ENV.itemDownloadPath,
        params: {
          itemUri: uri
        }
      }).then(function(response){
        deferred.resolve(response.data);
      }, function(err) {
        deferred.reject(err);
      });

      return deferred.promise;
    }

  });
