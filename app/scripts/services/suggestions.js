'use strict';

/**
 * @ngdoc service
 * @name micoApp.suggestions
 * @description
 * # suggestions
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('suggestion', function (NAMESPACES) {

    var suggestRegex = new RegExp("\\s+([A-Za-z]+):([A-Za-z]+)$",'ig');

    this.checkSuggestion = function(cm) {
      var c = cm.getCursor();
      var line = cm.getRange({'line': c.line, 'ch': 0},{'line': c.line, 'ch': c.ch});
      var match = suggestRegex.exec(line);
      if(match) {

        var replace = function(replacement,from,to,uri,prefix) {
          cm.setSelection(from, cm.getCursor());
          cm.replaceSelection(replacement);
          c.ch = c.ch + replacement.length;
          cm.setCursor(c);

          var regex = new RegExp("PREFIX\\s+" + prefix + ":\\s*<",'ig');

          //if prefix is not yet defined
          if(!cm.getValue().match(regex)) {
            c.line = c.line + 1;
            cm.setValue("PREFIX " + prefix + ": <" + uri+">\n" + cm.getValue());
            cm.setCursor(c);
          }
        };

        var showHint = function(from, to, list) {
          CodeMirror.showHint(cm, function() {
            return {
              list: list,
              from: from,
              to: to
            };
          },{
            completeSingle: false
          });
        };

        //check if is in the local store
        var result;

        //check if it is in static
        for(var property in NAMESPACES) {
          if(NAMESPACES[property] === match[1]) {
            result = property;
            break;
          }
        }

        if(result === undefined) {
          return;
        }

        var query = result+match[2];

        var pushToList = function(list,r,from,to) {
          list.push({
            text: r,
            hint: function() {
              replace(" "+r+" ",from, to, result,match[1]);
            }
          });
        };

        jQuery.ajax("http://lov.okfn.org/dataset/lov/api/v2/autocomplete/terms?q=" + encodeURIComponent(query), {
          async: false,
          dataType: "json",
          success: function(data) {

            var list = [];

            var from = {line: c.line, ch: c.ch - match[0].length};
            var to = {line: c.line, ch: c.ch};

            for(var i = 0; i < data.results.length; i++) {
              var r = match[1]+":"+data.results[i].localName;

              if(query === r) {
                list = [];
                break;
              }

              pushToList(list,r,from,to);
            }

            showHint(from, to, list);
          }
        });
      }
    };

    /**
     * Autocompletion using prefix.cc
     * @param cm
     */
    this.checkAutocomplete = function(cm) {

      var c = cm.getCursor();
      var line = cm.getRange({'line': c.line, 'ch': 0},{'line': c.line, 'ch': c.ch});
      if(line[line.length - 1] === ':') {
        //get prefix
        var prefix = /.*[\s.,;\{\}]([^:]+):$/g.exec(line)[1];

        var text = cm.getValue();

        var regex = new RegExp("PREFIX\\s+" + prefix + ":\\s*<",'ig');

        //if prefix is not yet defined
        if(!text.match(regex)) {

          CodeMirror.showHint(cm, function(cm) {

            var result;

            //check if it is in static
            for(var property in NAMESPACES) {
              if(NAMESPACES[property] === prefix) {
                result = property;
                break;
              }
            }

            if(result === undefined) {
              try {
                jQuery.ajax('http://prefix.cc/' + prefix + '.file.json', {
                  async: false,
                  success: function(data) {
                    result = data[prefix];
                    NAMESPACES[result] = prefix;
                  },
                  dataType: "json"
                });
              } catch (e) {}
            }

            if (result !== undefined) {
              return {
                list: [{
                  text: "add prefix " + prefix + ": <" + result + ">",
                  hint: function() {

                    var regex = new RegExp(".*(PREFIX\\s+" + prefix + ":)$",'ig');

                    if( line.match(regex) ) {
                      var replacement = " <" + result + ">";
                      cm.replaceSelection(replacement);
                      c.ch = c.ch + replacement.length;
                      cm.setCursor(c);
                    } else {
                      c.line = c.line + 1;
                      cm.setValue("PREFIX " + prefix + ": <" + result+">\n" + cm.getValue());
                      cm.setCursor(c);
                    }
                  }

                }],
                from: {line: c.line, ch: c.ch - prefix.length},
                to: {line: c.line, ch: c.ch}
              };
            }

          },{
            completeSingle: false
          });
        } else {
          //get suggestions for prefix


          /*for(var property in NAMESPACES) {
           if(NAMESPACES[property] == prefix) {
           getSuggestions(property);
           }
           }*/
        }
      }
    };
  });
