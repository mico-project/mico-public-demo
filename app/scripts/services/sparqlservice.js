'use strict';

/**
 * @ngdoc service
 * @name micoApp.SparqlService
 * @description
 * # SparqlService
 * Service in the micoApp.
 */
angular.module('micoApp')
  .service('SparqlService', function ($q,$http,ENV,$rootScope) {

    function toResult(data,properties,filters,singleItemLists) {
      var result = [];
      angular.forEach(data.results.bindings, function(binding){
        var res = {};
        angular.forEach(properties, function(property){
          if(filters && filters[property]) {
            res[property] = filters[property](binding[property].value);
          } else {
            res[property] = binding[property].value;
          }
        });
        result.push(res);
      });
      if(result.length == 0) return undefined;
      if(result.length == 1) {
        if(properties.length == 1) {
          return singleItemLists ? result : result[0][properties[0]];
        } else {
          return singleItemLists ? result : result[0];
        }
      }
      return result;
    }

    this.query = function(label, query, properties,filters,singleItemLists) {
      var deferred = $q.defer();

      $rootScope.$broadcast('request',{type:'Sparql',label:label,query:query});

      $http({
        method: 'POST',
        url: ENV.sparqlPath,
        headers: {
          "Content-Type":"application/sparql-query",
          "Accept":"application/sparql-results+json"
        },
        data: query
      }).then(function(response){
        var result = toResult(response.data,properties,filters,singleItemLists);
        deferred.resolve(result);
      });

      return deferred.promise;
    };

    this.raw_query = function(query, type, onsuccess, onfailure) {

      $http({
        url: ENV.sparqlPath,
        method: "POST",
        data: query,
        headers: {
          'Content-Type': 'application/sparql-query' + (document.charset ? (";charset=" + document.charset) : ""),
          'Accept': type
        }
      })
        .then(function(data) {
          onsuccess(data.data,data.status, data.headers, data.config);
        }, function(data) {
          onfailure(data.data,data.status, data.headers, data.config);
        });
    };

  });
