'use strict';

/**
 * @ngdoc filter
 * @name micoApp.filter:urlstring
 * @function
 * @description
 * # urlstring
 * Filter in the micoApp.
 */
angular.module('micoApp')
  .filter('urlstring', function () {
    return function (input) {
      return input.replace(/.*\//g,'')
    };
  });
