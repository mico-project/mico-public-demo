'use strict';

/**
 * @ngdoc filter
 * @name micoApp.filter:firstsentence
 * @function
 * @description
 * # firstsentence
 * Filter in the micoApp.
 */
angular.module('micoApp')
  .filter('firstsentence', function () {
    return function (input) {
      return input.substring(0, input.indexOf('. ')+1);
    };
  });
