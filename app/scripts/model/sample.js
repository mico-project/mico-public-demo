'use strict';

/**
 * @ngdoc service
 * @name micoApp.Sample
 * @description
 * # Sample
 * Factory in the micoApp.
 */
angular.module('micoApp')
  .factory('Sample', function () {

    function Sample(uri,type,img) {
      this.uri = uri;
      this.type = type;
      this.img = img;
    }

    return Sample;
  });
