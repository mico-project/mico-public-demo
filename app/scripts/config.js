"use strict";

/* ATTENTION: FILE IS AUTO-GENERATED, CHANGES WILL BE LOST */

angular.module('config', [])

.constant('ENV', {listWorkflowsPath:'http://demo3.mico-project.eu/mico-workflow-management-service/list/workflows',getWorkflowPath:'http://demo3.mico-project.eu/mico-workflow-management-service/get/workflow/ui-params',statusWorkflowPath:'http://demo3.mico-project.eu/mico-workflow-management-service/get/workflow/status',fileUploadPath:'http://demo3.mico-project.eu/rest/upload/file',urlUploadPath:'http://demo3.mico-project.eu/rest/upload/url',contentUploadPath:'http://demo3.mico-project.eu/rest/upload/content',sparqlPath:'http://demo3.mico-project.eu/rest/sparql/select',statusServicePath:'http://demo3.mico-project.eu/rest/status',itemSubmitPath:'http://demo3.mico-project.eu/rest/submit',itemDownloadPath:'http://demo3.mico-project.eu/rest/download'})

;