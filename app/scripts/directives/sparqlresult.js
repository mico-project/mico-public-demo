'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:sparqlResult
 * @description
 * # sparqlResult
 */
angular.module('micoApp')
  .directive('sparqlResult', function ($compile) {
    return {
      restrict: 'E',
      link: function(scope, elem){
        scope.$watch("result",function(newValue,oldValue) {
          if(newValue !== oldValue) {
            var htm = '<'+scope.writer.id+' result="result" run-query="runQuery"></'+scope.writer.id+'>';
            var compiled = $compile(htm)(scope);
            elem.html(compiled);
          }
        });
      }
    };
  });
