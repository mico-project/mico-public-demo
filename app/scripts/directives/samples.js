'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:samples
 * @description
 * # samples
 */
angular.module('micoApp')
  .directive('samples', function ($rootScope,$location) {
    return {
      template: '<div class="samples row"></div>',
      scope: {
        model: '='
      },
      replace:true,
      restrict: 'E',
      link: function postLink(scope, element, attrs) {

        function getIconClass(type) {
          switch(type) {
            case 'image': return "glyphicon-picture";
            case 'video': return "glyphicon-play";
            case 'text': return "glyphicon-align-justify";
            default: return "";
          }
        }

        angular.forEach(scope.model, function(sample){
           var sampleImg = angular.element('<div>')
             .css('backgroundImage','url("'+sample.img+'")')
             .addClass('col-md-2')
             .addClass('sample-image')
             .append('<i class="sample-overlay glyphicon '+getIconClass(sample.type)+'">')
             .on('click',function() {
               $location.path('/result/'+sample.type).search({}).search('uri',sample.uri);
               $rootScope.$apply()
             });
           element.append(sampleImg);
        })

      }
    };
  });
