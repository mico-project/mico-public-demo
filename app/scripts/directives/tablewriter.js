'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:tableWriter
 * @description
 * # tableWriter
 */
angular.module('micoApp')
  .directive('tableWriter', function (NAMESPACES) {
    return {
      templateUrl: 'views/sparqltable.html',
      scope: {
        result: '=',
        runQuery: "="
      },
      restrict: 'E',
      link: function(scope, element) {

        var pageSize = 10;

        var query_for_resource = "SELECT DISTINCT ?property ?hasValue ?isValueOf WHERE {\n  { ###URI### ?property ?hasValue }\nUNION\n  { ?isValueOf ?property ###URI### }\n}\nORDER BY ?property ?hasValue ?isValueOf\nLIMIT 1000";
        var query_for_property = "SELECT DISTINCT ?resource ?value WHERE {\n  ?resource ###URI### ?value\n}\nORDER BY ?resource ?value\nLIMIT 1000";
        var query_for_class = "SELECT DISTINCT ?instance WHERE {\n  ?instance a ###URI###\n}\nORDER BY ?instance\nLIMIT 1000";

        function getQuery(uri, name) {

          var ns_uri = "<" + uri + ">";

          switch (name) {
            case 'property':
              return query_for_property.replace(/###URI###/g, ns_uri);//.replace(/###NSP###/g,namespace).replace(/###NAME###/g,ns_name);
            case 'class':
              return query_for_class.replace(/###URI###/g, ns_uri);//.replace(/###NSP###/g,namespace).replace(/###NAME###/g,ns_name);
            case 'type':
              return query_for_class.replace(/###URI###/g, ns_uri);//.replace(/###NSP###/g,namespace).replace(/###NAME###/g,ns_name);
            default :
              return query_for_resource.replace(/###URI###/g, ns_uri);//.replace(/###NSP###/g,namespace).replace(/###NAME###/g,ns_name);
          }

        }

        var getDisplayName = function (uri) {

          if (!uri) return "";

          var namespaceIndex = Math.max(uri.lastIndexOf("/"), uri.lastIndexOf("#")) + 1;
          var namespace = uri.substring(0, namespaceIndex);

          if (namespace in NAMESPACES) {
            return NAMESPACES[namespace] + ":" + uri.substring(namespaceIndex);
          } else {
            return uri;
          }
        };

        var getDatatypeForBinding = function (binding) {
          if (binding && binding.datatype) {
            return getDisplayName(binding.datatype);
          }
        };

        var getTitleForBinding = function (binding) {
          return binding.value + (binding['xml:lang'] ? '@' + binding['xml:lang'] : '') + (binding.datatype ? '^^' + binding.datatype : "");
        };

        var bindings;
        var offset;

        function drawData() {
          var showBindings = bindings.slice(offset, offset + pageSize);

          scope.firstItem = offset + 1;
          scope.lastItem = offset + showBindings.length;

          scope.showPrev = offset > 0;
          scope.showNext = offset + showBindings.length < bindings.length;

          //$anchorScroll();

          scope.bindings = showBindings;
        }

        scope.getDisplayName = getDisplayName;

        scope.headers = scope.result.head.vars;
        bindings = scope.result.results.bindings;

        scope.selectURI = function (uri, name) {
          var query = getQuery(uri, name);
          scope.runQuery(query);
        };

        scope.prev = function () {
          offset = Math.max(0, offset - pageSize);
          drawData();
        };

        scope.next = function () {
          offset = offset + pageSize < scope.resultSize ? offset + pageSize : offset;
          drawData();
        };

        scope.getTitleForBinding = getTitleForBinding;

        scope.getDatatypeForBinding = getDatatypeForBinding;

        scope.resultSize = bindings.length;

        offset = 0;

        drawData();
      }
    };
  });
