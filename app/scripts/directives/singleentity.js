'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:singleentity
 * @description
 * # singleentity
 */
angular.module('micoApp')
  .directive('singleentity', function ($http) {
    return {
      template: '<div class="entity"><div class="row"><div class="col-md-7"><h1 ng-bind="entity.label"></h1><h2 ng-if="entity.type">Type: <span ng-bind="entity.type | urlstring"></span></h2><h2>Confidence: <span ng-bind="entity.confidence"></span></h2><h3><span ng-bind="entity.uri"></span></h3><p ng-if="abstract" ng-bind="abstract | firstsentence"></p></div><div class="col-md-5"><img style="width: 100%" ng-if="image" ng-src="{{image}}"></div></div></div>',
      restrict: 'E',
      replace: true,
      scope: {
        entity: '='
      },
      link: function postLink(scope, element, attrs) {
        if(scope.entity.uri.indexOf('http://dbpedia.org/') == 0) {
          $http.get('http://dbpedia.org/sparql',{params:{
            query:'SELECT ?image ?abstract WHERE {<'+scope.entity.uri+'> <http://dbpedia.org/ontology/abstract> ?abstract. OPTIONAL {<'+scope.entity.uri+'> <http://xmlns.com/foaf/0.1/depiction> ?image} FILTER langMatches(lang(?abstract),\'en\')}',
            format:'application/json',
            'default-graph-uri':'http://dbpedia.org'
          }}).then(function(data){
            if(data.data.results.bindings.length > 0) {
              scope.image = data.data.results.bindings[0].image ? data.data.results.bindings[0].image.value : undefined;
              scope.abstract = data.data.results.bindings[0].abstract.value;
            }
          }, function(data){
            console.log(data);
            console.error('cannot get dbpedia data');
          })
        }
      }
    };
  });
