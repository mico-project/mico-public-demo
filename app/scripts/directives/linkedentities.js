'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:linkedEntities
 * @description
 * # linkedEntities
 */
angular.module('micoApp')
  .directive('linkedEntities', function () {
    return {
      template: '<div></div>',
      restrict: 'E',
      replace: true,
      scope: {
        text: '=',
        entities: '=',
        current: '='
      },
      link: function postLink(scope, element, attrs) {

        function createLink(entity) {
          var link = angular.element('<span class="link" title="'+entity.type+'">'+entity.label+' </span><span  class="conf">'+entity.confidence+'</span>');
          link.click(function(){
            scope.current = entity;
          });
          return link;
        }

        scope.$watch('entities',function(n){
          if(n != undefined) {

            var start_and_length = [];

            angular.forEach(n, function(entity){
              start_and_length[entity.start] = {length:entity.end-entity.start,entity:entity};
            });

            var text = [];
            for(var i = 0; i < scope.text.length; i++) {
              if(start_and_length[i]) {
                element.append(text.join(""));
                element.append(createLink(start_and_length[i].entity));
                i += start_and_length[i].length - 1;
                text = [];
              } else {
                text.push(scope.text[i]);
              }
            }
          }
        });
      }
    };
  });
