'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:jsonWriter
 * @description
 * # jsonWriter
 */
angular.module('micoApp')
  .directive('jsonWriter', function () {
    return {
      template: '<pre></pre>',
      restrict: 'E',
      link: function postLink(scope, element) {
        element.find('pre').text(angular.toJson(scope.result,true));
      }
    };
  });

