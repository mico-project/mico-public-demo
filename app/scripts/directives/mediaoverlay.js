'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:mediaOverlay
 * @description
 * # mediaOverlay
 */
angular.module('micoApp')
  .directive('mediaOverlay', function ($filter) {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {

        var elems = angular.element('<div style="position: relative;z-index:1000;"></div>');

        scope.$watch(attrs["ngShow"],function(newValue,oldValue) {
          if(newValue === false) elems.empty();
        });

        element.before(elems);

        function getColor(type) {
          switch(type) {
            case 'animals': return 'lightgreen';
            default: return 'lightgray';
          }
        }

        element.bind('load', function () {

          scope.display = true;
          scope.$apply();

          var factor = element.width()/element[0].naturalWidth;

          var overlayObject = scope[attrs['mediaOverlay']];

          angular.forEach(overlayObject, function (overlay, color) {

            var elem = angular.element("<div></div>");
            elem.css({
              border: "4px solid",
              borderColor: getColor(overlay.type),
              position: "absolute",
              top: Math.floor(overlay.region.y*factor) + "px",
              left: Math.floor(overlay.region.x*factor) + "px",
              width: Math.floor(overlay.region.w*factor) + "px",
              height: Math.floor(overlay.region.h*factor) + "px"
            });
            if(overlay.label) {

              var conf = overlay.confidence ? (' ('+$filter('number')(overlay.confidence, 2)+')') : "";

              elem.append('<span style="font-size:10px;padding: 0 2px;position: absolute;left:0;bottom:0;background-color: '+getColor(overlay.type)+'">'+overlay.label+conf+"</span>");
            }
            elems.append(elem);
          });
        });
      }
    }
  });
