'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:xmlWriter
 * @description
 * # xmlWriter
 */
angular.module('micoApp')
  .directive('xmlWriter', function () {
    return {
      template: '<pre></pre>',
      restrict: 'E',
      link: function postLink(scope, element) {
        element.find('pre').text(scope.result);
      }
    };
  });
