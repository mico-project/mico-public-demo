'use strict';

/**
 * @ngdoc directive
 * @name micoApp.directive:imageload
 * @description
 * # imageload
 */
angular.module('micoApp')
  .directive('imageload', function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('load', function() {
          scope.display = true;
          scope.$apply();
        });
        element.bind('canplay', function() {
          scope.display = true;
          scope.$apply();
        });
      }
    };
  });
