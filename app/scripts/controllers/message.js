'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:MessageCtrl
 * @description
 * # MessageCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('MessageCtrl', function (MessageService, $scope) {

    $scope.addMessage = function(text, timeout) {
      MessageService.addMessage(text,'danger',timeout);
    };


    $scope.closeMessage = function(message) {
        MessageService.removeMessage(message);
      }

  });
