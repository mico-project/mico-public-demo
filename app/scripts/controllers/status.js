'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:StatusCtrl
 * @description
 * # StatusCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('StatusCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
