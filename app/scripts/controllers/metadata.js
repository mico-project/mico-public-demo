'use strict';

/**
 * @ngdoc function
 * @name micoPublicDemoApp.controller:MetadataCtrl
 * @description
 * # MetadataCtrl
 * Controller of the micoPublicDemoApp
 */
angular.module('micoApp')
  .controller('MetadataCtrl', function (SparqlService, suggestion, $scope, $rootScope, $log, $location) {

    function codemirrorloaded(editor){
      editor.on("keyup",function(i,e){
        if(e.keyCode === 16) {
          suggestion.checkAutocomplete(i);
        } else {
          if(e.keyCode === 37 || e.keyCode === 38|| e.keyCode === 39 || e.keyCode === 40) {
            return;
          }
          suggestion.checkSuggestion(i);
        }
      });
    }

    //codemirror setup
    $scope.editorOptions = {
      lineWrapping : true,
      viewportMargin: 2000,
      lineNumbers: true,
      mode: 'sparql',
      theme: 'mdn-like sparql-mm',
      onLoad: codemirrorloaded
    };

    //writers
    $scope.writers = [
      {"id":"table-writer","name":"Table","type":"application/sparql-results+json"},
      {"id":"json-writer","name":"JSON","type":"application/sparql-results+json"},
      {"id":"xml-writer","name":"XML","type":"application/sparql-results+xml"}
    ];

    //samples
    $scope.sparql_samples = [
      {"name":"Select first 20 triples", "value":"SELECT * WHERE {\n  ?subject ?property ?object\n} LIMIT 20","writer":$scope.writers[0]},
      {"name":"List video ts for specific terms", "value":"PREFIX mm: <http://linkedmultimedia.org/sparql-mm/ns/1.0.0/function#>\nPREFIX mico: <http://www.mico-project.eu/ns/platform/1.0/schema#>\nPREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX oa: <http://www.w3.org/ns/oa#>\nPREFIX dc: <http://purl.org/dc/elements/1.1/>\nPREFIX dct: <http://purl.org/dc/terms/>\nPREFIX fam: <http://vocab.fusepool.info/fam#>\n\nSELECT ?c ?timestamp ?stt_value WHERE {\n  ?c mico:hasContentPart ?cp .\n  ?cp mico:hasContent ?stt_annot .\n  ?stt_annot oa:hasBody ?stt_body .\n  ?stt_body rdf:type  ?stt_body_type .\n  ?stt_body rdf:value ?stt_value .\n  ?stt_annot oa:hasTarget ?tgt .\n  ?tgt  oa:hasSelector ?fs .\n  ?fs rdf:value ?timestamp .\n  FILTER (?stt_body_type = mico:STTBody)\n  FILTER regex(?stt_value,\"emissions\")\n}","writer":$scope.writers[1]}
    ];

    $scope.selectSample = function(sample) {
      $scope.query = sample.value;
      $scope.run(sample.writer);
    };

    $scope.runQuery = function(query) {
      $scope.query = query;
      $scope.run();
    };

    //querying
    $scope.run = function(writer) {

      if(writer === undefined) {
        if($scope.writer === undefined) {
          writer = $scope.writers[0];
        } else {
          writer = $scope.writer;
        }
      }

      $scope.alert = undefined;

      if(!$scope.query || $scope.query === "") {
        return false;
      }

      $location.search({query:$scope.query});

      $scope.loader = true;

      SparqlService.raw_query(
        $scope.query.trim(),
        writer.type,
        function(data){
          $scope.loader = false;

          $scope.result = data;
          $scope.writer = writer;

        }, function(data) {
          $scope.loader = false;
          $log.error(data);
          $scope.alert({title:"Error",msg:data});
        }
      );

    };

    if($location.search().query) {
      $scope.query = $location.search().query;
      $scope.run();
    }

  });
