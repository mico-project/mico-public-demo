'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:RequestviewCtrl
 * @description
 * # RequestviewCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('RequestviewCtrl', function ($scope, $rootScope,$interval) {

    $scope.requests = [];

    var req = [];

    var timeout = $interval(function(){
      $scope.requests = req.slice(0,50);
      req = req.slice(0,51);
    },2000);

    function push(type,label,data,curl) {
      var date = new Date();
      req.unshift(
        {
          type:type,
          label:label,
          data:data,
          time:date
        }
      )
    }

    $rootScope.$on('request',function(event,data){
      push(data.type,data.label,data.query)
    });

    $scope.getLink = function(request) {
      switch(request.type) {
        case 'Sparql': return '#!/metadata?query='+encodeURIComponent(request.data);
        case 'Content': return request.data;
        default: return "";
      }
    }

  });
