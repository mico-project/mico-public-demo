'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:TextCtrl
 * @description
 * # TextCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('TextCtrl', function ($scope,$location,$timeout,TextAnalysisService,StatusService,MessageService) {

    var uri = $location.search().uri;

    var timeout;
    function check() {
      StatusService.getStatus(uri).then(function(status){
        if(status.finished != "false") {
          $scope.finished = true;
          TextAnalysisService.getText(uri).then(function(text){$scope.text = text}).then(TextAnalysisService.getAnalysisResult(uri).then(function(result) {
            $scope.result = result;
          }));
        } else {
          console.log("retry in 1 sec");
          $scope.finished = false;
          timeout = $timeout(check,1000);
        }
      }, function(err){
        MessageService.addMessage("Cannot get data: " + (err.data ? err.data.message : err.statusText), "danger", 3000);
      });
    }

    if(uri) {
      check();
    } else {
      MessageService.addMessage("URI has to be defined","danger",3000);
    }

    $scope.$on('$destroy', function () { if(timeout) $timeout.cancel(timeout); });


  });
