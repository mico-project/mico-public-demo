'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:ImageCtrl
 * @description
 * # ImageCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('ImageCtrl', function ($scope,$location,ImageAnalysisService,StatusService,MessageService,$timeout,ENV,$rootScope) {

    var uri = $location.search().uri;

    function createImageUrl() {
      $rootScope.$broadcast('request',{type:'Content',label:'Get Image',query:ENV.itemDownloadPath + '?itemUri='+encodeURIComponent(uri)});
      return ENV.itemDownloadPath + '?itemUri=' + encodeURIComponent(uri);
    }

    function drawResult(data) {
      $scope.loading = false;
      var results = [];

      angular.forEach(data,function(result,index){
        if(result) {
          angular.forEach(result, function(r){
            results.push(
              {type:index,label:r.label,region:r.region,confidence:r.confidence}
            );
          })
        }
      });

      $scope.imageSrc = createImageUrl();
      $scope.imageOverlay = results;
    }

    var timeout;
    function check() {
      StatusService.getStatus(uri).then(function(status){
        if(status.finished != "false") {
          $scope.finished = true;
          ImageAnalysisService.getAnalysisResult(uri).then(function(result) {
            drawResult(result);
          });
        } else {
          console.log("retry in 3 sec");
          $scope.finished = false;
          timeout = $timeout(check,3000);
        }
      }, function(err){
        MessageService.addMessage("Cannot get data: " + (err.data ? err.data.message : err.statusText), "danger", 3000);
      })
    }

    if(uri) {
      check();
    } else {
      MessageService.addMessage("URI has to be defined","danger",3000);
    }

    $scope.$on('$destroy', function () { if(timeout) $timeout.cancel(timeout); });

  });
