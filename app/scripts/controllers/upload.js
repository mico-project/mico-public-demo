'use strict';

/**
 * @ngdoc function
 * @name micoPublicDemoApp.controller:UploadCtrl
 * @description
 * # UploadCtrl
 * Controller of the micoPublicDemoApp
 */
angular.module('micoApp')
  .controller('UploadCtrl', function (WorkflowService,MessageService,$location,$scope,$http,Upload, ENV) {

    $scope.insertText = false;

    $scope.showProgress = false;

    $scope.file = undefined;

    var workflowId = $location.search().workflow || '1';

    function getTypes(workflow) {
      var userNode = [];

      angular.forEach(workflow.nodes, function(node, index){
          if(node.id == "@--- MICO User ---@") userNode.push(index);
      });

      if(userNode.length > 0) {

        var result = [];

        for(var i in workflow.links) {
          if(userNode.indexOf(workflow.links[i].source) > -1) {

            var exists = false;
            for(var j in result) {
              if(result[j].syntacticType == workflow.links[i].syntacticType) {
                exists = true;
              }
            }

            if(!exists) {
              result.push({
                mimetypes: workflow.links[i].mimeTypeList.split(","),
                syntacticType: workflow.links[i].syntacticType,
                inputTypes: workflow.links[i].mimeTypeList.split(",")[0].split("/")[0]
              })
            }
          }
        }

        if(result.length == 1) {
          return result[0];
        } else {
          MessageService.addMessage("Multipart Uploads currently not implemented", "danger", 3000);
        }
      } else {
        MessageService.addMessage("Cannot get starting point of workflow", "danger", 3000);
      }

      MessageService.addMessage("Cannot parse workflow", "danger", 3000);

      return undefined;
    }

    WorkflowService.getWorkflow(workflowId).then(function (data) {

      $scope.workflow = data;
      var types = getTypes(data);

      if(types) {
        $scope.mimetype = types.mimetypes;
        $scope.inputType = types.inputTypes;
        $scope.syntacticType = types.syntacticType;

        MessageService.addMessage("Workflow '"+$scope.workflow.workflowName+"' is build for analyzing content of type " + $scope.mimetype.join(", "),'info',5000);
      }

    }, function (error) {
      console.error(error);
    });

    function redirect(uri) {
      $location.path('/result/'+$scope.inputType).search({'uri':uri});
    }

    function submit(itemURL) {
      $http({
        method: 'POST',
        url: ENV.itemSubmitPath,
        params: {item:itemURL,route:workflowId}
      }).then(function successCallback(response) {
        redirect(itemURL);
      }, function errorCallback(response) {
        MessageService.addMessage("Submission failed: " + (response.data ? response.data.message : "No message"), "danger", 3000);
      });
    }

    function upload(email) {

      $scope.loadPhotodata = false;

      var file = $scope.file;

      if(!file) return MessageService.addMessage("Select a file first","warning",3000);

      if (!file.$error) {

        $scope.progress = 0;
        $scope.showProgress = true;

        var obj = {
          url: ENV.fileUploadPath,
          data: file,
          headers : {
            'Content-Type': file.type
          }
        };

        obj.params = {
          type:$scope.syntacticType,
          name:file.name
        };

        if(email) obj.params.email = email;

        Upload.http(obj).then(function successCallback(response) { //TODO should be multipart
          $scope.showProgress = false;
          submit(response.data.itemUri);
        }, function errorCallback(response){
          MessageService.addMessage("Upload failed: " + (response.data ? response.data.message : "No message"), "danger", 3000);
        }, function progressCallback(evt) {
          $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
      }
    }

    $scope.$watch('file', function () {if($scope.file)
      if ($scope.file != null && $scope.inputType != 'video') {
        upload();
      }
    });

    $scope.upload = function () {
      upload();
    };

    $scope.loadFromURL = function(url) {
      if(!url) return MessageService.addMessage("Set url of the item you want to use","warning",3000);

      //TODO
      return MessageService.addMessage("Upload from URL is currently not active","warning",3000);

      $http({
        method: 'POST',
        url: ENV.urlUploadPath,
        params: {url:$scope.uri}
      }).then(function successCallback(response) {
        redirect(response.data.uri);
      }, function errorCallback(response) {
        MessageService.addMessage("Upload failed: " + (response.data ? response.data.message : "No message"), "danger", 3000);
      });

    };

    function uploadContent(data,type,encoding) {

      //TODO
      return MessageService.addMessage("Upload content directly is currently not active","warning",3000);

      $http({
        method: 'POST',
        url: ENV.contentUploadPath,
        data: data,
        headers: {
          'Content-Type':type,
          'Content-Transfer-Encoding':encoding
        }
      }).then(function successCallback(response) {
        redirect(response.data.uri);
      }, function errorCallback(response) {
        MessageService.addMessage("Upload failed: " + (response.data ? response.data.message : "No message"), "danger", 3000);
      });
    }

    $scope.loadText = function(text) {
      if(!text) return MessageService.addMessage("Write a text first, then push the send button","warning",3000);

      uploadContent(text,'text/plain',"8BIT")

    };

    $scope.loadFileWithEmail = function(email) {
      upload(email);
    };

    $scope.cameraEnabled = false;

    $scope.loadFromCamera = function() {
      $scope.loadPhotodata = true;
      $scope.$apply();
      $scope.cameraEnabled = false;

      uploadContent($scope.photodata,'image/jpeg',"BASE64")
    };


  });
