'use strict';

/**
 * @ngdoc function
 * @name micoPublicDemoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the micoPublicDemoApp
 */
angular.module('micoApp')
  .controller('MainCtrl', function ($scope,$location,WorkflowService) {

    WorkflowService.listWorkflows().then(function(workflows){
      $scope.workflows = workflows;
    });

    $scope.onSelected = function(workflow) {
      $location.path('/upload').search('workflow',workflow.id);
    }

  });
