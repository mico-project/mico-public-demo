'use strict';

/**
 * @ngdoc function
 * @name micoApp.controller:VideoCtrl
 * @description
 * # VideoCtrl
 * Controller of the micoApp
 */
angular.module('micoApp')
  .controller('VideoCtrl', function ($scope,$location,VideoAnalysisService,StatusService,MessageService,$timeout,ENV,$rootScope) {

    var uri = $location.search().uri;

    $scope.uri = uri;

    $scope.getVideoUrl = function() {
      return ENV.itemDownloadPath + "?itemUri=" + uri;
    };

    $scope.getSegmentImgUrl = function(segment) {
      return ENV.itemDownloadPath + "?partUri=" + encodeURIComponent(segment.image) + "&itemUri=" + encodeURIComponent(uri);
    };

    function drawResult(data) {
      $scope.segments = data.segments;

      $rootScope.$broadcast('request',{type:'Content',label:'Get Video',query:ENV.itemDownloadPath + "?itemUri=" + uri});
      angular.forEach(data.segments,function(segment){
        $rootScope.$broadcast('request',{type:'Content',label:'Get Video Segment',query:ENV.itemDownloadPath + "?partUri=" + encodeURIComponent(segment.image) + "&itemUri=" + encodeURIComponent(uri)});
      })
    }

    var timeout;
    function check() {
      StatusService.getStatus(uri).then(function(status){
        if(status.finished != "false") {
          $scope.finished = true;
          VideoAnalysisService.getAnalysisResult(uri).then(function(result) {
            drawResult(result);
          });
        } else {
          console.log("retry in 3 sec");
          $scope.finished = false;
          timeout = $timeout(check,3000);
        }
      }, function(err){
        MessageService.addMessage("Cannot get data: " + (err.data ? err.data.message : err.statusText), "danger", 3000);
      })
    }

    if(uri) {
      check();
    } else {
      MessageService.addMessage("URI has to be defined","danger",3000);
    }

    $scope.$on('$destroy', function () { if(timeout) $timeout.cancel(timeout); });
  });
