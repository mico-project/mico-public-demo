"use strict"

var myAppDev = angular.module('micoAppE2E', ['micoApp', 'ngMockE2E']);

myAppDev.run(function($httpBackend,ENV) {

  var workflowIds = ['10','11','12'];
  var workflows = [
    {"workflowName":"Image Test Chain (DEMO)","nodes":[{"id":"@--- MICO User ---@","description":"MICO user, ingesting the input content"},{"id":"FaceDetectionImage","description":"MICO service for detection of faces in images."},{"id":"@--- MICO System ---@","description":"MICO system, storing all the results produced by the current configuration"}],"links":[{"source":0,"target":1,"syntacticType":"mico:Image","mimeTypeList":"image/jpeg,image/png"},{"source":1,"target":2,"syntacticType":"mmmterms:FaceDetectionBody","mimeTypeList":"application/x-mico-rdf"},{"source":1,"target":2,"syntacticType":"mico:FaceDetectionXML","mimeTypeList":"text/vnd.fhg-ccv-facedetection+xml"}]},
    {"workflowName":"Text Test Chain (DEMO)","nodes":[{"id":"@--- MICO User ---@","description":"MICO user, ingesting the input content"},{"id":"FaceDetectionImage","description":"MICO service for detection of faces in images."},{"id":"@--- MICO System ---@","description":"MICO system, storing all the results produced by the current configuration"}],"links":[{"source":0,"target":1,"syntacticType":"mico:Text","mimeTypeList":"text/plain"},{"source":1,"target":2,"syntacticType":"mmmterms:FaceDetectionBody","mimeTypeList":"application/x-mico-rdf"},{"source":1,"target":2,"syntacticType":"mico:FaceDetectionXML","mimeTypeList":"text/vnd.fhg-ccv-facedetection+xml"}]},
    {"workflowName":"Video Test Chain (DEMO)","nodes":[{"id":"@--- MICO User ---@","description":"MICO user, ingesting the input content"},{"id":"FaceDetectionImage","description":"MICO service for detection of faces in images."},{"id":"@--- MICO System ---@","description":"MICO system, storing all the results produced by the current configuration"}],"links":[{"source":0,"target":1,"syntacticType":"mico:Video","mimeTypeList":"video/mp4"},{"source":1,"target":2,"syntacticType":"mmmterms:FaceDetectionBody","mimeTypeList":"application/x-mico-rdf"},{"source":1,"target":2,"syntacticType":"mico:FaceDetectionXML","mimeTypeList":"text/vnd.fhg-ccv-facedetection+xml"}]}
  ];

  $httpBackend.whenGET(new RegExp(ENV.getWorkflowPath + "(.+)")).respond(function(method,url, data, headers, params) {
    var result;
    var id = url.substring(url.lastIndexOf('/')+1);
    try {
      var workflow = workflows[parseInt(id)-10];
      if(workflow) result = [200, workflow, {}];
    } catch(e) {

    }
    if(!result) result = [404, 'Not found', {}];

    return result;
  });
  $httpBackend.whenGET(ENV.listWorkflowsPath+"?user=mico").respond(workflowIds);
  $httpBackend.whenGET(/^views\//).passThrough();

  $httpBackend.whenPOST(ENV.sparqlPath).passThrough();
  $httpBackend.whenPOST(ENV.fileUploadPath).respond({uri:'http://ex.org/123'});
  $httpBackend.whenPOST(new RegExp(ENV.fileUploadPath+"\\?email=.*")).respond({uri:'http://demo3.mico-project.eu:8080/marmotta/5121a382-6206-443b-9025-7d9bbdcbff60'});
  $httpBackend.whenPOST(ENV.urlUploadPath).respond({uri:'http://ex.org/123'});
  $httpBackend.whenPOST(ENV.contentUploadPath).respond({uri:'http://ex.org/123'});
});
