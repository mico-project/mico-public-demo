'use strict';

/**
 * @ngdoc overview
 * @name micoPublicDemoApp
 * @description
 * # micoPublicDemoApp
 *
 * Main module of the application.
 */
angular
  .module('micoApp', [
    'ngResource',
    'ngRoute',
    'ngAnimate',
    'ngTouch',
    'config',
    'ui.bootstrap',
    'ui.select',
    'angular.filter',
    'ngSanitize',
    'ngFileUpload',
    'ui.codemirror'
  ])
  .config(function ($routeProvider,$sceDelegateProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/result', {
        templateUrl: 'views/result.html',
        controller: 'ResultCtrl',
        controllerAs: 'result'
      })
      .when('/upload', {
        templateUrl: 'views/upload.html',
        controller: 'UploadCtrl',
        controllerAs: 'upload'
      })
      .when('/workflows', {
        templateUrl: 'views/workflows.html',
        controller: 'WorkflowsCtrl',
        controllerAs: 'workflows'
      })
      .when('/metadata', {
        templateUrl: 'views/metadata.html',
        controller: 'MetadataCtrl',
        controllerAs: 'metadata'
      })
      .when('/result/video', {
        templateUrl: 'views/video.html',
        controller: 'VideoCtrl',
        controllerAs: 'video'
      })
      .when('/result/image', {
        templateUrl: 'views/image.html',
        controller: 'ImageCtrl',
        controllerAs: 'image'
      })
      .when('/result/text', {
        templateUrl: 'views/text.html',
        controller: 'TextCtrl',
        controllerAs: 'text'
      })
      .when('/status', {
        templateUrl: 'views/status.html',
        controller: 'StatusCtrl',
        controllerAs: 'status'
      })
      .otherwise({
        redirectTo: '/'
      });

    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      'http://demo3.mico-project.eu/**', //for testing
      'http://demo3.mico-project.eu:8080/**', //for testing
      'http://static.greenpeace.org/**'
    ]);

  }).constant('NAMESPACES', {
    "http://www.w3.org/2000/01/rdf-schema#":"rdfs",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#":"rdf",
    "http://www.w3.org/2004/02/skos/core#":"skos",
    "http://xmlns.com/foaf/0.1/":"foaf",
    "http://purl.org/dc/terms/":"dct",
    "http://www.w3.org/ns/ma-ont#":"ma",
    "http://purl.org/dc/elements/1.1/":"dc",
    "http://www.w3.org/2001/XMLSchema#":"xsd"
  }).run(function($rootScope, SampleService){
    //TODO create random samples once for the whole page
    $rootScope.samples = SampleService.listSamples();
  });
