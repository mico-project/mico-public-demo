'use strict';

describe('Service: NamedEntityResult', function () {

  // load the service's module
  beforeEach(module('micoApp'));

  // instantiate service
  var NamedEntityResult;
  beforeEach(inject(function (_NamedEntityResult_) {
    NamedEntityResult = _NamedEntityResult_;
  }));

  it('should do something', function () {
    expect(!!NamedEntityResult).toBe(true);
  });

});
