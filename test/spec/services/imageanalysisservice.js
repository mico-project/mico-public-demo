'use strict';

describe('Service: ImageAnalysisService', function () {

  // load the service's module
  beforeEach(module('micoApp'));

  // instantiate service
  var ImageAnalysisService;
  beforeEach(inject(function (_ImageAnalysisService_) {
    ImageAnalysisService = _ImageAnalysisService_;
  }));

  it('should do something', function () {
    expect(!!ImageAnalysisService).toBe(true);
  });

});
