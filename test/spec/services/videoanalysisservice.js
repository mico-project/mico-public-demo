'use strict';

describe('Service: VideoAnalysisService', function () {

  // load the service's module
  beforeEach(module('micoApp'));

  // instantiate service
  var VideoAnalysisService;
  beforeEach(inject(function (_VideoAnalysisService_) {
    VideoAnalysisService = _VideoAnalysisService_;
  }));

  it('should do something', function () {
    expect(!!VideoAnalysisService).toBe(true);
  });

});
