'use strict';

describe('Service: TextService', function () {

  // load the service's module
  beforeEach(module('micoApp'));

  // instantiate service
  var TextService;
  beforeEach(inject(function (_TextService_) {
    TextService = _TextService_;
  }));

  it('should do something', function () {
    expect(!!TextService).toBe(true);
  });

});
