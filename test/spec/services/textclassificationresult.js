'use strict';

describe('Service: TextClassificationResult', function () {

  // load the service's module
  beforeEach(module('micoApp'));

  // instantiate service
  var TextClassificationResult;
  beforeEach(inject(function (_TextClassificationResult_) {
    TextClassificationResult = _TextClassificationResult_;
  }));

  it('should do something', function () {
    expect(!!TextClassificationResult).toBe(true);
  });

});
