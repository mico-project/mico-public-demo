'use strict';

describe('Controller: WorkflowsCtrl', function () {

  // load the controller's module
  beforeEach(module('micoPublicDemoApp'));

  var WorkflowsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WorkflowsCtrl = $controller('WorkflowsCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(WorkflowsCtrl.awesomeThings.length).toBe(3);
  });
});
