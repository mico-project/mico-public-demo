'use strict';

describe('Controller: RequestviewCtrl', function () {

  // load the controller's module
  beforeEach(module('micoApp'));

  var RequestviewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequestviewCtrl = $controller('RequestviewCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RequestviewCtrl.awesomeThings.length).toBe(3);
  });
});
