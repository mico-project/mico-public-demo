'use strict';

describe('Filter: urlstring', function () {

  // load the filter's module
  beforeEach(module('micoApp'));

  // initialize a new instance of the filter before each test
  var urlstring;
  beforeEach(inject(function ($filter) {
    urlstring = $filter('urlstring');
  }));

  it('should return the input prefixed with "urlstring filter:"', function () {
    var text = 'angularjs';
    expect(urlstring(text)).toBe('urlstring filter: ' + text);
  });

});
