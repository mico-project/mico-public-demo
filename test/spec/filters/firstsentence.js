'use strict';

describe('Filter: firstsentence', function () {

  // load the filter's module
  beforeEach(module('micoApp'));

  // initialize a new instance of the filter before each test
  var firstsentence;
  beforeEach(inject(function ($filter) {
    firstsentence = $filter('firstsentence');
  }));

  it('should return the input prefixed with "firstsentence filter:"', function () {
    var text = 'angularjs';
    expect(firstsentence(text)).toBe('firstsentence filter: ' + text);
  });

});
