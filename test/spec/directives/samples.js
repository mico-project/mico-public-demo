'use strict';

describe('Directive: samples', function () {

  // load the directive's module
  beforeEach(module('micoApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<samples></samples>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the samples directive');
  }));
});
