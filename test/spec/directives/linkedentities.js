'use strict';

describe('Directive: linkedEntities', function () {

  // load the directive's module
  beforeEach(module('micoApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<linked-entities></linked-entities>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the linkedEntities directive');
  }));
});
