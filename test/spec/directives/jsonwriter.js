'use strict';

describe('Directive: jsonWriter', function () {

  // load the directive's module
  beforeEach(module('micoApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<json-writer></json-writer>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the jsonWriter directive');
  }));
});
